package com.github.ramannada.kadesubmissiondua.viewmodel


import android.view.View
import com.github.ramannada.kadesubmissiondua.R
import com.github.ramannada.kadesubmissiondua.model.League
import com.github.ramannada.kadesubmissiondua.view.league.LeagueAdapter

/**
 * Created by labibmuhajir on 24/10/18.
 * labibmuhajir@yahoo.com
 */
class ListLeagueViewModel: BaseViewModel(){
    val adapter: LeagueAdapter = LeagueAdapter()
    private var leagues: MutableList<League> = mutableListOf()
    override var errorClickListener = View.OnClickListener { initData() }

    init {
        initData()
    }

    private fun initData() {
        val idLeague = context.resources.getIntArray(R.array.id_league)
        val leagueName = context.resources.getStringArray(R.array.league_name)
        val leagueBadge = context.resources.obtainTypedArray(R.array.league_badge)

        leagues.clear()
        for (i in idLeague.indices) {
            leagues.add(League(idLeague[i],
                    leagueName[i],
                    leagueBadge.getResourceId(i, 0)))
        }

        adapter.setListLeague(leagues)
        leagueBadge.recycle()
    }
}