package com.github.ramannada.kadesubmissiondua.viewmodel


import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import androidx.viewpager.widget.ViewPager
import com.github.ramannada.kadesubmissiondua.model.League
import com.github.ramannada.kadesubmissiondua.networking.response.LeagueDetailResponse
import com.github.ramannada.kadesubmissiondua.view.leaguedetail.LeagueDetailActivity
import com.github.ramannada.kadesubmissiondua.view.match.MatchAdapter
import com.github.ramannada.kadesubmissiondua.view.match.MatchPagerAdapter
import com.google.android.material.tabs.TabLayout
import retrofit2.Call
import retrofit2.Response
import kotlin.reflect.KClass

/**
 * Created by labibmuhajir on 24/10/18.
 * labibmuhajir@yahoo.com
 */
class LeagueViewModel(private val idLeague: Int, fragmentManager: FragmentManager): BaseViewModel() {
    private val id = MutableLiveData<Int>()
    private val leagueName = MutableLiveData<String>()
    private val leagueBadge = MutableLiveData<Int>()
    private val leagueBadgeUrl = MutableLiveData<String>()
    private val leagueFacebook = MutableLiveData<String>()
    private val leagueTwitter = MutableLiveData<String>()
    val pagerAdapter: MatchPagerAdapter = MatchPagerAdapter(fragmentManager)
    lateinit var tabLayout: TabLayout
    lateinit var viewPager: ViewPager

    init {
        load()
    }

    fun bind(league: League?) {
        league?.let {
            id.value = it.id
            leagueName.value = it.name
            leagueBadge.value = it.badge
            leagueBadgeUrl.value = it.badgeUrl
            leagueFacebook.value = it.facebook
            leagueTwitter.value = it.twitter
        }
    }

    private fun load() {
        api.leagueDetail(idLeague)
                .enqueue(object : retrofit2.Callback<LeagueDetailResponse> {
                    override fun onFailure(call: Call<LeagueDetailResponse>, t: Throwable) {
                        errorMessage.value = t.message
                    }

                    override fun onResponse(call: Call<LeagueDetailResponse>, response: Response<LeagueDetailResponse>) {
                        if (!response.isSuccessful) {
                            errorMessage.value = response.message()
                            return
                        }

                        response.body()?.let {
                            if(it.league.isNotEmpty()) {
                                bind(it.league[0])
                                return
                            }

                            errorMessage.value = "empty response"
                        }
                    }

                })
    }

    private fun openLeagueDetail() {
        val intent = MutableLiveData<Pair<KClass<*>, Bundle?>>()
        val bundle = Bundle()


        intent.value = Pair(LeagueDetailActivity::class, bundle)

        activityToStart = intent
    }

    fun getLeagueId(): MutableLiveData<Int> {
        return id
    }

    fun getLeagueName(): MutableLiveData<String> {
        return leagueName
    }

    fun getLeagueBadge(): MutableLiveData<Int> {
        return leagueBadge
    }

    fun getLeagueBadgeUrl(): MutableLiveData<String> {
        return leagueBadgeUrl
    }

    fun getLeagueFacebook(): MutableLiveData<String> {
        return leagueFacebook
    }

    fun getLeagueTwitter(): MutableLiveData<String> {
        return leagueTwitter
    }
}