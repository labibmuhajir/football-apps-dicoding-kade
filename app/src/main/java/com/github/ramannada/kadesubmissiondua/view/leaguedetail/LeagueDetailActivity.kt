package com.github.ramannada.kadesubmissiondua.view.leaguedetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.github.ramannada.kadesubmissiondua.R
import com.github.ramannada.kadesubmissiondua.databinding.LeagueDetailActivityBinding
import com.github.ramannada.kadesubmissiondua.model.League
import com.github.ramannada.kadesubmissiondua.util.ConstantHelper
import com.github.ramannada.kadesubmissiondua.viewmodel.LeagueViewModel
import com.github.ramannada.kadesubmissiondua.viewmodelfactory.LeagueViewModelFactory
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import androidx.databinding.BindingAdapter
import com.github.ramannada.kadesubmissiondua.view.match.MatchPagerAdapter


class LeagueDetailActivity : AppCompatActivity() {
    lateinit var binding: LeagueDetailActivityBinding
    lateinit var viewModel: LeagueViewModel
    lateinit var league: League

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.league_detail_activity)


        league = intent.getParcelableExtra(ConstantHelper.LEAGUE)

        viewModel = ViewModelProviders.of(this, LeagueViewModelFactory(league.id, supportFragmentManager))
                .get(LeagueViewModel::class.java)

        viewModel.bind(league)

        binding.viewModel = viewModel

    }


    fun getLeagueId(): Int {
        return league.id
    }
}
