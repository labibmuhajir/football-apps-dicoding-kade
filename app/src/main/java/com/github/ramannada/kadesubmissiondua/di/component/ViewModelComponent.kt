package com.github.ramannada.kadesubmissiondua.di.component

import com.github.ramannada.kadesubmissiondua.di.module.NetworkModule
import com.github.ramannada.kadesubmissiondua.viewmodel.BaseViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Created by labibmuhajir on 24/10/18.
 * labibmuhajir@yahoo.com
 */

@Component(modules = [NetworkModule::class],
        dependencies = [AppComponent::class])
interface ViewModelComponent {
    fun inject(baseViewModel: BaseViewModel)
}