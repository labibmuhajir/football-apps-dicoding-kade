package com.github.ramannada.kadesubmissiondua.util


import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.github.ramannada.kadesubmissiondua.extension.getParentActivity
import com.squareup.picasso.Picasso
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.annotation.NonNull
import com.google.android.material.tabs.TabLayout



/**
 * Created by labibmuhajir on 24/10/18.
 * labibmuhajir@yahoo.com
 */

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}

@BindingAdapter("pagerAdapter")
fun setPagerAdapter(view: ViewPager, adapter: PagerAdapter) {
    view.adapter = adapter
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()

    if (parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value -> view.text = value?:"" })
    }
}

@BindingAdapter("mutableInt")
fun setMutableInt(view: TextView, int: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()

    if (parentActivity != null && int != null) {
        int.observe(parentActivity, Observer { value -> view.text = value?.toString()?:"" })
    }
}

@BindingAdapter("imageSrc")
fun setImageViewResource(imageView: ImageView, resource: String) {
    Picasso.get().load(resource).into(imageView)
}

@BindingAdapter("imageSrc")
fun setImageViewResource(imageView: ImageView, resource: Int) {
    Picasso.get().load(resource).into(imageView)
}