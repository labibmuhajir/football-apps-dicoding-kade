package com.github.ramannada.kadesubmissiondua.viewmodel

import android.util.Log
import android.view.View
import com.github.ramannada.kadesubmissiondua.model.Match
import com.github.ramannada.kadesubmissiondua.networking.response.MatchesResponse
import com.github.ramannada.kadesubmissiondua.util.ConstantHelper
import com.github.ramannada.kadesubmissiondua.view.match.MatchAdapter
import retrofit2.Call
import retrofit2.Response

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */
class ListMatchViewModel(private val id: Int?, private val status: String?): BaseViewModel() {
    val adapter: MatchAdapter = MatchAdapter()
    private var matches: MutableList<Match> = mutableListOf()
    override var errorClickListener = View.OnClickListener{initData()}

    init {
        initData()
        Log.d("list match view model", adapter.toString())
    }

    private fun initData() {
        id?.let {
            when(status) {
                ConstantHelper.LAST_MATCH -> {
                    api.listLastMatch(it).enqueue(object : retrofit2.Callback<MatchesResponse>{
                        override fun onFailure(call: Call<MatchesResponse>, t: Throwable) {
                            errorMessage.value = t.message
                        }

                        override fun onResponse(call: Call<MatchesResponse>, response: Response<MatchesResponse>) {
                            if (!response.isSuccessful) {
                                errorMessage.value = response.message()
                                return
                            }

                            response.body()?.let {
                                matches.addAll(it.matches)
                                return
                            }

                            errorMessage.value = "response body null"
                        }
                    })
                }

                ConstantHelper.NEXT_MATCH -> {
                    api.listNextMatch(it).enqueue(object : retrofit2.Callback<MatchesResponse>{
                        override fun onFailure(call: Call<MatchesResponse>, t: Throwable) {
                            errorMessage.value = t.message
                        }

                        override fun onResponse(call: Call<MatchesResponse>, response: Response<MatchesResponse>) {
                            if (!response.isSuccessful) {
                                errorMessage.value = response.message()
                                return
                            }

                            response.body()?.let {
                                matches.addAll(it.matches)
                                adapter.setListMatch(matches)
                                return
                            }

                            errorMessage.value = "response body null"
                        }
                    })
                }
            }
        }
    }

    fun getMatches(): MutableList<Match> {
        return matches
    }
}