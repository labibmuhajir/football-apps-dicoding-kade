package com.github.ramannada.kadesubmissiondua.view.match


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.ramannada.kadesubmissiondua.R
import com.github.ramannada.kadesubmissiondua.databinding.MatchLastFragmentBinding
import com.github.ramannada.kadesubmissiondua.util.ConstantHelper
import com.github.ramannada.kadesubmissiondua.view.leaguedetail.LeagueDetailActivity
import com.github.ramannada.kadesubmissiondua.viewmodel.ListMatchViewModel
import com.github.ramannada.kadesubmissiondua.viewmodelfactory.ListMatchViewModelFactory


class MatchLastFragment : Fragment() {
    lateinit var binding: MatchLastFragmentBinding
    lateinit var viewModel: ListMatchViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.match_last_fragment,
                container, false)
        binding.rvLastMatch.layoutManager = LinearLayoutManager(context)

        viewModel = ViewModelProviders.of(this,
        ListMatchViewModelFactory((activity as LeagueDetailActivity).getLeagueId(),
                ConstantHelper.LAST_MATCH)).get(ListMatchViewModel::class.java)

        binding.viewModel = viewModel
        Log.d("match last fragment", binding.rvLastMatch.adapter.toString())

        return binding.root
    }
}
