package com.github.ramannada.kadesubmissiondua.model

import com.google.gson.annotations.SerializedName

/**
 * Created by labibmuhajir on 26/10/18.
 * labibmuhajir@yahoo.com
 */
data class Team (
        @SerializedName("idTeam")
        var id: Int,
        @SerializedName("strTeam")
        var name: String,
        @SerializedName("strLeague")
        var league: String,
        @SerializedName("strManager")
        var manager: String,
        @SerializedName("strStadium")
        var stadium: String,
        @SerializedName("strStadiumThumb")
        var stadiumThumbnail: String,
        @SerializedName("strDescriptionEN")
        var description: String
)