package com.github.ramannada.kadesubmissiondua.di.module

import com.github.ramannada.kadesubmissiondua.BuildConfig
import com.github.ramannada.kadesubmissiondua.networking.api.LeagueApi
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by labibmuhajir on 24/10/18.
 * labibmuhajir@yahoo.com
 */

@Module
object NetworkModule {
    @Provides
    fun gson(): Gson {
        return Gson()
    }

    @Provides
    fun okHttp(): OkHttpClient{
        return OkHttpClient().newBuilder()
                .connectTimeout(BuildConfig.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(BuildConfig.READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(BuildConfig.WRITE_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY)
                ).build()
    }

    @Provides
    fun retrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    @Provides
    fun leagueApi(retrofit: Retrofit): LeagueApi {
        return retrofit.create(LeagueApi::class.java)
    }
}