package com.github.ramannada.kadesubmissiondua.networking.api

import com.github.ramannada.kadesubmissiondua.networking.response.LeagueDetailResponse
import com.github.ramannada.kadesubmissiondua.networking.response.MatchesResponse
import com.github.ramannada.kadesubmissiondua.networking.response.TeamResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by labibmuhajir on 24/10/18.
 * labibmuhajir@yahoo.com
 */
interface LeagueApi {
    @GET("lookupleague.php")
    fun leagueDetail(@Query("id") idLeague: Int?): Call<LeagueDetailResponse>

    @GET("eventspastleague.php")
    fun listLastMatch(@Query("id") idLeague: Int?): Call<MatchesResponse>

    @GET("eventsnextleague.php")
    fun listNextMatch(@Query("id") idLeague: Int?): Call<MatchesResponse>

    @GET("lookupevent.php?")
    fun matchDetail(@Query("id") idMatch: Int?): Call<MatchesResponse>

    @GET("lookupteam.php")
    fun teamDetail(@Query("id") idTeam: Int?): Call<TeamResponse>
}