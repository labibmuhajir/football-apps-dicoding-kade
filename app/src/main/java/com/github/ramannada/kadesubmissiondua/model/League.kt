package com.github.ramannada.kadesubmissiondua.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by labibmuhajir on 24/10/18.
 * labibmuhajir@yahoo.com
 */
@Parcelize
data class League(
        @SerializedName("idLeague")
        var id: Int,
        @SerializedName("strLeague")
        var name: String,
        var badge: Int?,
        @SerializedName("strBadge")
        var badgeUrl: String = "",
        @SerializedName("strFacebook")
        var facebook: String = "",
        @SerializedName("strTwitter")
        var twitter: String = ""
) : Parcelable