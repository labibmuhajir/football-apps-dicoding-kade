package com.github.ramannada.kadesubmissiondua.viewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.github.ramannada.kadesubmissiondua.viewmodel.ListMatchViewModel


/**
 * Created by labibmuhajir on 26/10/18.
 * labibmuhajir@yahoo.com
 */
class ListMatchViewModelFactory(private val id: Int, private val status: String): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ListMatchViewModel(id, status) as T
    }
}