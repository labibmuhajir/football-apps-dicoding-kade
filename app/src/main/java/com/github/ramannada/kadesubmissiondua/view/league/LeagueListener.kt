package com.github.ramannada.kadesubmissiondua.view.league

import com.github.ramannada.kadesubmissiondua.model.League

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */
interface LeagueListener {
    fun onClick(league: League)

    fun onLongClick(league: League)
}