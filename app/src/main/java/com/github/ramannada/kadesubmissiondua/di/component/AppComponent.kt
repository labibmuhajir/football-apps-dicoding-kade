package com.github.ramannada.kadesubmissiondua.di.component

import android.content.Context
import com.github.ramannada.kadesubmissiondua.di.module.AppModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */

@Component(modules = [AppModule::class])
interface AppComponent {
    fun context(): Context
}