package com.github.ramannada.kadesubmissiondua.viewmodel

import androidx.lifecycle.MutableLiveData
import com.github.ramannada.kadesubmissiondua.model.Match
import com.github.ramannada.kadesubmissiondua.networking.response.MatchesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */
class MatchViewModel(private val idMatch: Int): BaseViewModel() {
    private val id = MutableLiveData<Int>()
    private val homeTeam = MutableLiveData<String>()
    private val awayTeam = MutableLiveData<String>()
    private val date = MutableLiveData<String>()
    private val homeScore = MutableLiveData<Int>()
    private val awayScore = MutableLiveData<Int>()
    private val league = MutableLiveData<String>()
    private val homeShot = MutableLiveData<Int>()
    private val homeGoalDetail = MutableLiveData<String>()
    private val homeYellowCard = MutableLiveData<String>()
    private val homeRedCard = MutableLiveData<String>()
    private val homeGoalKeeper = MutableLiveData<String>()
    private val homeDefender = MutableLiveData<String>()
    private val homeMidfielder = MutableLiveData<String>()
    private val homeForwarder = MutableLiveData<String>()
    private val homeSubt = MutableLiveData<String>()
    private val homeFormation = MutableLiveData<String>()
    private val awayShot = MutableLiveData<Int>()
    private val awayGoalDetail = MutableLiveData<String>()
    private val awayYellowCard = MutableLiveData<String>()
    private val awayRedCard = MutableLiveData<String>()
    private val awayGoalKeeper = MutableLiveData<String>()
    private val awayDefender = MutableLiveData<String>()
    private val awayMidfielder = MutableLiveData<String>()
    private val awayForwarder = MutableLiveData<String>()
    private val awaySubt = MutableLiveData<String>()
    private val awayFormation = MutableLiveData<String>()


    fun bind(match: Match?) {
        match?.let {
            id.value = it.id
            homeTeam.value = it.homeTeam
            awayTeam.value = it.awayTeam
            date.value = it.date
            homeScore.value = it.homeScore
            awayScore.value = it.awayScore
            league.value = it.league
            homeShot.value = it.homeShot
            homeGoalDetail.value = it.homeGoalDetail
            homeYellowCard.value = it.homeYellowCard
            homeRedCard.value = it.homeRedCard
            homeGoalKeeper.value = it.homeGoalKeeper
            homeDefender.value = it.homeDefender
            homeMidfielder.value = it.homeMidfielder
            homeForwarder.value = it.homeForwarder
            homeSubt.value = it.homeSubt
            homeFormation.value = it.homeFormation
            awayShot.value = it.awayShot
            awayGoalDetail.value = it.awayGoalDetail
            awayYellowCard.value = it.awayYellowCard
            awayRedCard.value = it.awayRedCard
            awayGoalKeeper.value = it.awayGoalKeeper
            awayDefender.value = it.awayDefender
            awayMidfielder.value = it.awayMidfielder
            awayForwarder.value = it.awayForwarder
            awaySubt.value = it.awaySubt
            awayFormation.value = it.awayFormation
        }
    }

    init {
        load()
    }

    private fun load() {
        api.matchDetail(idMatch).enqueue(object : Callback<MatchesResponse>{
            override fun onFailure(call: Call<MatchesResponse>, t: Throwable) {
                errorMessage.value = t.message
            }

            override fun onResponse(call: Call<MatchesResponse>, response: Response<MatchesResponse>) {
                if (!response.isSuccessful) {
                    errorMessage.value = response.message()
                    return
                }

                response.body()?.let {
                    bind(it.matches[0])
                }
            }

        })
    }

    fun getId(): MutableLiveData<Int> {
        return id
    }

    fun getHomeTeam(): MutableLiveData<String> {
        return  homeTeam
    }

    fun getAwayTeam(): MutableLiveData<String> {
        return awayTeam
    }

    fun getDate(): MutableLiveData<String> {
        return date
    }

    fun getHomeScore(): MutableLiveData<Int> {
        return homeScore
    }

    fun getHomeShot(): MutableLiveData<Int> {
        return homeShot
    }

    fun getAwayScore(): MutableLiveData<Int> {
        return awayScore
    }

    fun getAwayShot(): MutableLiveData<Int> {
        return awayShot
    }

    fun getLeague(): MutableLiveData<String> {
        return league
    }

    fun getHomeGoalDetail(): MutableLiveData<String> {
        return homeGoalDetail
    }

    fun getHomeYellowCard(): MutableLiveData<String> {
        return homeYellowCard
    }

    fun getHomeRedCard(): MutableLiveData<String> {
        return homeRedCard
    }

    fun getHomeGoalKeeper(): MutableLiveData<String> {
        return homeGoalKeeper
    }

    fun getHomeDefender(): MutableLiveData<String> {
        return homeDefender
    }

    fun getHomeMidFielder(): MutableLiveData<String> {
        return homeMidfielder
    }

    fun getHomeForwarder(): MutableLiveData<String> {
        return homeForwarder
    }

    fun getHomeSubt(): MutableLiveData<String> {
        return homeSubt
    }

    fun getHomeFormation(): MutableLiveData<String> {
        return homeFormation
    }

    fun getAwayGoalDetail(): MutableLiveData<String> {
        return awayGoalDetail
    }

    fun getAwayYellowCard(): MutableLiveData<String> {
        return awayYellowCard
    }

    fun getAwayRedCard(): MutableLiveData<String> {
        return awayRedCard
    }

    fun getAwayGoalKeeper(): MutableLiveData<String> {
        return awayGoalKeeper
    }

    fun getAwayDefender(): MutableLiveData<String> {
        return awayDefender
    }

    fun getAwayMidFielder(): MutableLiveData<String> {
        return awayMidfielder
    }

    fun getAwayForwarder(): MutableLiveData<String> {
        return awayForwarder
    }

    fun getAwaySubt(): MutableLiveData<String> {
        return awaySubt
    }

    fun getAwayFormation(): MutableLiveData<String> {
        return awayFormation
    }
}