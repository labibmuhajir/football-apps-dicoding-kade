package com.github.ramannada.kadesubmissiondua.view.match

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.github.ramannada.kadesubmissiondua.R
import com.github.ramannada.kadesubmissiondua.databinding.MatchViewHolderBinding
import com.github.ramannada.kadesubmissiondua.model.Match
import com.github.ramannada.kadesubmissiondua.viewmodel.MatchViewModel

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */
class MatchAdapter: RecyclerView.Adapter<MatchAdapter.MatchViewHolder>() {
    private var listMatch: List<Match> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        val binding: MatchViewHolderBinding = DataBindingUtil.inflate(LayoutInflater
                .from(parent.context), R.layout.match_view_holder, parent, false)

        return MatchViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listMatch.size
    }

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bind(listMatch[position])
    }

    inner class MatchViewHolder(val binding: MatchViewHolderBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(match: Match) {
            val viewModel = MatchViewModel(match.id)
            viewModel.bind(match)

            binding.viewModel = viewModel
        }

    }

    fun setListMatch(match: List<Match>) {
        listMatch = match
        notifyDataSetChanged()
    }
}