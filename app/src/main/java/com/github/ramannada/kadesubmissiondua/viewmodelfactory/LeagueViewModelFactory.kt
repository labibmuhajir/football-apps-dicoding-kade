package com.github.ramannada.kadesubmissiondua.viewmodelfactory

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.github.ramannada.kadesubmissiondua.viewmodel.LeagueViewModel
import com.github.ramannada.kadesubmissiondua.viewmodel.ListMatchViewModel

/**
 * Created by labibmuhajir on 26/10/18.
 * labibmuhajir@yahoo.com
 */
class LeagueViewModelFactory(val id: Int, val fragmentManager: FragmentManager): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return LeagueViewModel(id, fragmentManager) as T
    }
}