package com.github.ramannada.kadesubmissiondua.view.match

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */
class MatchPagerAdapter(fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        when(position) {
            0 -> return MatchLastFragment()
            1 -> return MatchNextFragment()
        }

        return MatchNextFragment()
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when(position) {
            0 -> return "Last Match"
            1 -> return "Next Match"
        }
        return super.getPageTitle(position)
    }


}