package com.github.ramannada.kadesubmissiondua.view.league


import android.content.Intent
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.ramannada.kadesubmissiondua.R
import com.github.ramannada.kadesubmissiondua.databinding.LeagueActivityBinding
import com.github.ramannada.kadesubmissiondua.model.League
import com.github.ramannada.kadesubmissiondua.util.ConstantHelper
import com.github.ramannada.kadesubmissiondua.view.leaguedetail.LeagueDetailActivity
import com.github.ramannada.kadesubmissiondua.viewmodel.ListLeagueViewModel
import com.google.android.material.snackbar.Snackbar

class LeagueActivity : AppCompatActivity(), LeagueListener {

    lateinit var binding: LeagueActivityBinding
    lateinit var viewModel: ListLeagueViewModel
    var errorSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.league_activity)
        binding.rvListLeague.layoutManager = GridLayoutManager(this, 2)

        viewModel = ViewModelProviders.of(this).get(ListLeagueViewModel::class.java)
        viewModel.adapter.setOnClickListener(this)

        binding.viewModel = viewModel
        viewModel.errorMessage.observe(this, Observer {
            errorMessage -> if (errorMessage != null) showError(errorMessage) else hideError()
        })
    }

    private fun showError(@StringRes errorMessage:String){
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry) {viewModel.errorClickListener}
        errorSnackbar?.show()
    }

    private fun hideError(){
        errorSnackbar?.dismiss()
    }

    override fun onClick(league: League) {
        val i = Intent(this, LeagueDetailActivity::class.java)
        i.putExtra(ConstantHelper.LEAGUE, league)
        startActivity(i)
    }

    override fun onLongClick(league: League) {

    }
}
