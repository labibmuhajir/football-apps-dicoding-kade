package com.github.ramannada.kadesubmissiondua.networking.response

import com.github.ramannada.kadesubmissiondua.model.Team
import com.google.gson.annotations.SerializedName

/**
 * Created by labibmuhajir on 26/10/18.
 * labibmuhajir@yahoo.com
 */
data class TeamResponse (var teams: List<Team>)