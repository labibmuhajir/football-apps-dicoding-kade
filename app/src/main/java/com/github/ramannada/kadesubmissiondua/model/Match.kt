package com.github.ramannada.kadesubmissiondua.model

import com.google.gson.annotations.SerializedName

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */
data class Match (
        @SerializedName("idEvent")
        var id: Int,
        @SerializedName("strHomeTeam")
        var homeTeam: String,
        @SerializedName("strAwayTeam")
        var awayTeam: String,
        @SerializedName("strDate")
        var date: String,
        @SerializedName("intHomeScore")
        var homeScore: Int,
        @SerializedName("intAwayScore")
        var awayScore: Int,
        @SerializedName("strLeague")
        var league: String,
        @SerializedName("intHomeShots")
        var homeShot: Int,
        @SerializedName("strHomeGoalDetails")
        var homeGoalDetail: String,
        @SerializedName("strHomeYellowCards")
        var homeYellowCard: String,
        @SerializedName("strHomeRedCards")
        var homeRedCard: String,
        @SerializedName("strHomeLineupGoalkeeper")
        var homeGoalKeeper: String,
        @SerializedName("strHomeLineupDefense")
        var homeDefender: String,
        @SerializedName("strHomeLineupMidfield")
        var homeMidfielder: String,
        @SerializedName("strHomeLineupForward")
        var homeForwarder: String,
        @SerializedName("strHomeLineupSubstitutes")
        var homeSubt: String,
        @SerializedName("strHomeFormation")
        var homeFormation: String,
        @SerializedName("intAwayShots")
        var awayShot: Int,
        @SerializedName("strAwayGoalDetails")
        var awayGoalDetail: String,
        @SerializedName("strAwayYellowCards")
        var awayYellowCard: String,
        @SerializedName("strAwayRedCards")
        var awayRedCard: String,
        @SerializedName("strAwayLineupGoalkeeper")
        var awayGoalKeeper: String,
        @SerializedName("strAwayLineupDefense")
        var awayDefender: String,
        @SerializedName("strAwayLineupMidfield")
        var awayMidfielder: String,
        @SerializedName("strAwayLineupForward")
        var awayForwarder: String,
        @SerializedName("strAwayLineupSubstitutes")
        var awaySubt: String,
        @SerializedName("strAwayFormation")
        var awayFormation: String

)