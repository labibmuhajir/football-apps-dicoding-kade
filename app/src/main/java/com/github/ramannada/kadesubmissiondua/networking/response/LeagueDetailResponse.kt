package com.github.ramannada.kadesubmissiondua.networking.response

import com.github.ramannada.kadesubmissiondua.model.League
import com.google.gson.annotations.SerializedName

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */
data class LeagueDetailResponse (@SerializedName("leagues") var league: List<League>)