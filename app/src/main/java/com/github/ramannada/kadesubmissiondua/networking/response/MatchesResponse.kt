package com.github.ramannada.kadesubmissiondua.networking.response

import com.github.ramannada.kadesubmissiondua.model.Match
import com.google.gson.annotations.SerializedName

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */
data class MatchesResponse (
        @SerializedName("events")
        var matches: List<Match>
)