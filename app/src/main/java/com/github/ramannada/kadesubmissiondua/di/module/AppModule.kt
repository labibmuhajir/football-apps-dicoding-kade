package com.github.ramannada.kadesubmissiondua.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */

@Module
class AppModule(private val context: Context?) {
    @Provides
    fun context(): Context {
        return this.context!!
    }
}