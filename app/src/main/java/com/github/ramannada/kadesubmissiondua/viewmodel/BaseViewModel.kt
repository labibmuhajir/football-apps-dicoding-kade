package com.github.ramannada.kadesubmissiondua.viewmodel

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.ramannada.kadesubmissiondua.App.Companion.appComponent
import com.github.ramannada.kadesubmissiondua.di.component.DaggerViewModelComponent
import com.github.ramannada.kadesubmissiondua.di.component.ViewModelComponent
import com.github.ramannada.kadesubmissiondua.di.module.NetworkModule
import com.github.ramannada.kadesubmissiondua.networking.api.LeagueApi
import javax.inject.Inject
import kotlin.reflect.KClass

/**
 * Created by labibmuhajir on 24/10/18.
 * labibmuhajir@yahoo.com
 */
open class BaseViewModel: ViewModel() {
    @Inject
    lateinit var api: LeagueApi
    @Inject
    lateinit var context: Context

    private val component: ViewModelComponent = DaggerViewModelComponent.builder()
            .appComponent(appComponent())
            .networkModule(NetworkModule)
            .build()
    val errorMessage: MutableLiveData<String> = MutableLiveData()
    open var errorClickListener = View.OnClickListener{ }
    open var clickListener = View.OnClickListener {  }
    open var activityToStart = MutableLiveData<Pair<KClass<*>, Bundle?>>()

    init {
        component.inject(this)
    }
}