package com.github.ramannada.kadesubmissiondua.util

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */
object ConstantHelper {
    val LEAGUE_VIEW_MODEL = "league_view_model"
    val LEAGUE = "league"
    val NEXT_MATCH = "next match"
    val LAST_MATCH = "last match"
}