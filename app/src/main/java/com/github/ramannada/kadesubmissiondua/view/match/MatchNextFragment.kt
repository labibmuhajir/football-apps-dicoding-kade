package com.github.ramannada.kadesubmissiondua.view.match


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.ramannada.kadesubmissiondua.R
import com.github.ramannada.kadesubmissiondua.databinding.MatchNextFragmentBinding
import com.github.ramannada.kadesubmissiondua.util.ConstantHelper
import com.github.ramannada.kadesubmissiondua.view.leaguedetail.LeagueDetailActivity
import com.github.ramannada.kadesubmissiondua.viewmodel.ListMatchViewModel
import com.github.ramannada.kadesubmissiondua.viewmodelfactory.ListMatchViewModelFactory


class MatchNextFragment : Fragment() {
    lateinit var binding: MatchNextFragmentBinding
    lateinit var viewModel: ListMatchViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.match_next_fragment,
                container, false)
        binding.rvNextMatch.layoutManager = LinearLayoutManager(context)

        viewModel = ViewModelProviders.of(this,
                ListMatchViewModelFactory((activity as LeagueDetailActivity).getLeagueId(),
                        ConstantHelper.NEXT_MATCH)).get(ListMatchViewModel::class.java)
        binding.viewModel = viewModel

        Log.d("match next fragment", binding.rvNextMatch.adapter.toString())

        return binding.root
    }


}
