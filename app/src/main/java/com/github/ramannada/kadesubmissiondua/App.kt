package com.github.ramannada.kadesubmissiondua

import android.app.Application
import com.github.ramannada.kadesubmissiondua.di.component.AppComponent
import com.github.ramannada.kadesubmissiondua.di.component.DaggerAppComponent
import com.github.ramannada.kadesubmissiondua.di.module.AppModule

/**
 * Created by labibmuhajir on 25/10/18.
 * labibmuhajir@yahoo.com
 */
class App: Application() {

    companion object {
        @JvmField
        var appComponent: AppComponent? = null

        @JvmStatic
        fun appComponent(): AppComponent? {
            return appComponent
        }
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(applicationContext))
                .build()
    }
}