package com.github.ramannada.kadesubmissiondua.view.league

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.ramannada.kadesubmissiondua.R
import com.github.ramannada.kadesubmissiondua.databinding.LeagueViewHolderBinding
import com.github.ramannada.kadesubmissiondua.model.League
import com.github.ramannada.kadesubmissiondua.viewmodel.LeagueViewModel

/**
 * Created by labibmuhajir on 24/10/18.
 * labibmuhajir@yahoo.com
 */
class LeagueAdapter: RecyclerView.Adapter<LeagueAdapter.LeagueViewHolder>() {
    private var listLeague: List<League> = listOf()
    private lateinit var context: Context
    private lateinit var interactionListener: LeagueListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeagueViewHolder {
        context = parent.context

        val binding: LeagueViewHolderBinding = DataBindingUtil
                .inflate(LayoutInflater.from(context),
                        R.layout.league_view_holder, parent, false)
        return LeagueViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listLeague.size
    }

    override fun onBindViewHolder(holder: LeagueViewHolder, position: Int) {
        holder.bind(listLeague[position])
    }

    fun setListLeague(listLeague: List<League>) {
        this.listLeague = listLeague
        notifyDataSetChanged()
    }

    fun setOnClickListener(listener: LeagueListener) {
        this.interactionListener = listener
    }

    inner class LeagueViewHolder(val binding: LeagueViewHolderBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(league: League) {
            val viewModel = LeagueViewModel(league.id, (context as AppCompatActivity).supportFragmentManager)

            viewModel.bind(league)
            binding.viewModel = viewModel
            binding.rootView.setOnClickListener { interactionListener.onClick(league) }
        }
    }
}